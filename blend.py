"""
Program to blend 2 images together.

This program can be used for tinting image with a flag for example.

Example usage:
>>> from imageio import imsave
>>> tinted_image = blend('cat.jpg', 'estonia_vertical.png')
>>> imsave('blend_demo.png', tinted_image)

"""
import numpy as np
from imageio import imread, imsave
from skimage.transform import resize
import matplotlib.pyplot as plt


def blend(img_a, img_b, *, a_weight=1., b_weight=1.):
    """
    Blend 2 images together.

    Args:
        img_a:
            filename of primary image.
        img_b:
            filename of layer image, that is resized to img_a size and applied
            onto img_a.
        a_weight:
            Positive float, that specifies img_a weight in the output.
            Defaults to 1.0.
        b_weight:
            Positive float, that specifies img_b weight in the output.
            Defaults to 1.0.
    Returns:
        Image result from blending img_a and img_b together.
        image is represented as 3 dimensional np array of integers
        from 0-255 for rgb values of image.
    """
    img_a = imread(img_a)
    img_b = imread(img_b)

    height, width, _ = img_a.shape
    img_b = rezise_img(img_b, height, width)

    return _blend(img_a, img_b, a_weight=a_weight, b_weight=b_weight)


def _blend(img_a, img_b, *, a_weight=1., b_weight=1.):
    """
    Blend 2 images together.

    Args:
        img_a:
            3 dimensional np array of integers 0-255 for rbg values.
        img_b:
            3 dimensional np array of integers 0-255 for rbg values.
            images has to have same dimensions,
            so that img_b.shape == img_a.shape is True.
        a_weight:
            Positive float, that specifies img_a weight in the output.
            Defaults to 1.0.
        b_weight:
            Positive float, that specifies img_b weight in the output.
            Defaults to 1.0.
    Returns:
        Image result from blending img_a and img_b together.
        image is represented as 3 dimensional np array of integers
        from 0-255 for rgb values of image.
    """
    # remove fourth layer (opacity)
    img_a = img_a[:, :, :3]
    img_b = img_b[:, :, :3]
    total_weight = a_weight + b_weight

    # blend images with adding them together
    img_c = (img_a * a_weight + img_b * b_weight) / total_weight
    return np.uint8(img_c)


def rezise_img(img, height, width):
    """
    Resize image to given height and width.

    Args:
        img: 3 dimensional np array of integers 0-255 for rbg values.
        height: desired height of the image.
        width: desired width of the image.
    Returns:
        Resized image as numpy array
    """
    resized_img = resize(
        img, (height, width),
        mode='constant', anti_aliasing=True
    )
    return np.uint8(resized_img * 255)


def make_plot_demo(save_fig=True):
    """
    Blend images and add results to matplotlib canvas.

    Helpful for testing weight levels.
    """
    img = imread('cat.jpg')
    img_flag = imread('estonia_vertical.png')

    height, width, _ = img.shape
    img_flag = rezise_img(img_flag, height, width)

    # Show the original image
    plt.subplot(1, 5, 1)
    plt.imshow(img)
    plt.title("cat")
    plt.axis('off')

    # Show blended images with different weight
    for a_weight, i in zip([1.5, 1, 0.5], range(2, 5)):
        img_blend = _blend(img, img_flag, a_weight=a_weight)
        plt.subplot(1, 5, i)
        plt.imshow(img_blend)
        plt.title(f"blend\na={a_weight}")
        plt.axis('off')

    # Show the original flag
    plt.subplot(1, 5, 5)
    plt.imshow(img_flag)
    plt.title("flag")
    plt.axis('off')

    if save_fig:
        plt.savefig("blending_plot_demo.png", dpi=150, bbox_inches='tight')
    plt.show()


def make_output_file_demo():
    img_blend = blend('cat.jpg', 'estonia_vertical.png')
    imsave('blend_demo.png', img_blend)


if __name__ == '__main__':
    make_plot_demo()
    make_output_file_demo()

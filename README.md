# Homework 3 image tinting
> Kristjan Tärk

This program can be used for tinting/blending two images together.
Images has to be the same dimensions. 
If dimensions are not the same then 
second image will be resized to first image dimensions.


## Requirements

For reading the image and resizing it scikit-image package is used.

`pip install scikit-image`

For blending images numpy is used for matrix calculations.

`pip install numpy`

For showing resulting images in a plot, matplotlib is used.

`pip install matplotlib`



## Demo

To blend cat and Estonian flag together we can use this libary like this

```python
from imageio import imsave # for saving the image
tinted_image = blend('cat.jpg', 'estonia_vertical.png')
imsave('est_cat.png', tinted_image)
```


![blending_plot_demo](blending_plot_demo.png)

You can also apply different blend levels to the images. This can be done with `a_weight` and `b_weight` arguments in the `blend` function. Weight default to 1.



To get cat and flag image with showing more cat, use higher weight for the cat:

`tinted_image = blend('cat.jpg', 'estonia_vertical.png', a_weight=1.5)`
